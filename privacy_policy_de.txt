DATENSCHUTZ-BESTIMMUNGEN

Öffi fordert die folgenden Berechtigungen an:

• Zugriff auf alle Netzwerke, weil Öffi die Auskunftssysteme nach
  Haltestellen, Abfahrten und Verspätungen (Livedaten) fragen muß.
• Standort, damit Öffi nahegelegene Haltestellen anzeigen kann und dich
  von deinem aktuellen Standort aus wegnavigieren kann. Das App-Widget
  benötigt die Standortberechtigung die ganze Zeit, während die App selbst
  sie nur während der Nutzung von Öffi benötigt.
• Kontakte, damit Öffi dich direkt zu oder von deinen Kontakten bringen
  kann.

Öffi nutzt oder sammelt Deine privaten Daten nicht für andere Zwecke als hier angegeben.

Es gibt keine Werbung und kein Tracking.
